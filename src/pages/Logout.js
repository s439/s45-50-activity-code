import {Navigate} from 'react-router-dom';
import {useEffect, useContext} from 'react';
import UserContext from '../UserContext';


export default function Logout(){

	const {unsetUser, setUser} = useContext(UserContext)
	//localStorage.clear() method will allow us to clear the information in the localStorage
	// localStorage.clear()
	unsetUser();

  //add a useEffect to run our setUser. This useEffect will have an empty dependency array.
  useEffect(()=>{
    setUser({
			// id: null,
			// isAdmin: null
      
      	email: null

    })
  }, [])

	return(

		<Navigate to="/login" />

		)

}