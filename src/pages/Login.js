import {useState, useEffect, useContext} from "react";
import {Navigate, Redirect} from "react-router-dom";
import {Form, Button} from "react-bootstrap";
import UserContext from '../UserContext';


export default function Login() {

  const {user, setUser} = useContext(UserContext);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(false);

  function authenticate(e) {
    //Prevents page redirection via form submission
    e.preventDefault();

    //Set the email from the authenticated user in the local storage
  /*
    SYNTAX:
      localStorage.setItem("propertyName", value)
  */
    localStorage.setItem("email", email)  
  
    setUser({
      email:localStorage.getItem('email')
    });
    
    setEmail("");
    setPassword("");

    alert("You are now logged in.");
    
  }

  useEffect(() => {
    if (email !== "" && password !== "" ) {
      setIsActive(true);
    } else {
      setIsActive(false)
    }
  }, [email,password])  

  return (
    (user.email !== null) ? 
      <Navigate to ="/courses" />
    :     <Form onSubmit = {(e) => authenticate(e)} >
    <h1>Login</h1>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control 
              type="email" 
              placeholder="Enter email"
              value = {email}
              onChange = {e => setEmail (e.target.value)} 
              required />
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="password">
        <Form.Label>Password</Form.Label>
        <Form.Control 
              type="password" 
              placeholder="Password"
              value = {password}
              onChange = {e => setPassword(e.target.value)}
              required />
      </Form.Group>


    {
        isActive ? 
          <Button variant="primary" type="submit" id="submitBtn">
          Login
        </Button>

      :
          <Button variant="primary" type="submit" id="submitBtn" disabled>
          Login
        </Button>     
    }


    </Form>

    
    
  )
}
