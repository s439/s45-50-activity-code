import PropTypes from 'prop-types'
import {Fragment} from 'react'
import CourseCard from '../components/CourseCard'
import coursesData from '../data/coursesData'
import {Container} from 'react-bootstrap'

export default function Courses() {	
	const courses = coursesData.map(course => {
		return (
			<CourseCard key={course.id} courseProp={course} />
		)		
	})

	return (		
		<Fragment>
			{courses}
		</Fragment>		
	)
}

CourseCard.prototype = {
	courseProp: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string,
		price: PropTypes.number.isRequired
	})
}