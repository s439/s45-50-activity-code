import React from 'react'
import Banner from '../components/Banner'

function PageNotFound(props) {
	return (
		<Banner isError = {true} />
	)
}

export default PageNotFound