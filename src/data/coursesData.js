const courseData = [
	{
		id: "wdc001",
		name: "PHP - Laravel",
		description: "Lorem ipsum dolor, sit, amet consectetur adipisicing elit. Veniam magni quisquam quaerat, accusamus sapiente repellat! Magnam numquam optio, ipsa, ullam iste tempore vero quisquam deserunt ex dolores tempora illum odio.",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python-Django",
		description: "Lorem ipsum dolor, sit, amet consectetur adipisicing elit. Veniam magni quisquam quaerat, accusamus sapiente repellat! Magnam numquam optio, ipsa, ullam iste tempore vero quisquam deserunt ex dolores tempora illum odio.",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java - Springboot",
		description: "Lorem ipsum dolor, sit, amet consectetur adipisicing elit. Veniam magni quisquam quaerat, accusamus sapiente repellat! Magnam numquam optio, ipsa, ullam iste tempore vero quisquam deserunt ex dolores tempora illum odio.",
		price: 55000,
		onOffer: true
	}
]

export default courseData