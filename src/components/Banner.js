// import Button from 'react-bootstrap/Button'
// import Row from 'react-bootstrap/Row'
// import Col from 'react-bootstrap/Col'

import {Button, Row, Col} from 'react-bootstrap'
import {Link} from 'react-router-dom'

export default function Banner(props) {

	return props.isError === false ? (
		<Row>
			<Col className="pt-5 pb-5">
				<h1>Zuitt Coding Bootstrap</h1>
				<p>Opportunities for everyone, anywhere</p>
				<Button variant="primary">Enroll Now!</Button>
			</Col>
		</Row>
	) : (
		<Row>
			<Col className="pt-5 pb-5">
				<h1>Page Not Found.</h1>
				<p>Go back to the{" "} 
					<Link as={Link} to='/'>homepage.</Link>
				</p>
			</Col>
		</Row>
	)
}