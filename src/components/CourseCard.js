import {useState, useEffect} from 'react'
import {Button, Card, Row, Col, Container} from 'react-bootstrap'

export default function CourseCard({courseProp}) {
	
	const {name, description, price} = courseProp
	/*
		Syntax:
			const [getter, setter] = useState(initialGetterValue)
	*/
	const [count, setCount] = useState(0)
	const [seat, setSeat] = useState(30)

	function enroll() {
		setCount(count + 1)
		setSeat(seat -1)
		
	}

	useEffect(() => {
		if (seat === 0) {
			alert ('No more seats available.')
		}
	}, [seat])

	return (
		<Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP {price}</Card.Text>
                <Card.Text>Enrollees: {count} </Card.Text>
                <Card.Text>Seats: {seat} </Card.Text>
                <Button variant="primary" onClick={enroll}>Enroll</Button>
            </Card.Body>
    	</Card>
	)
}

