

import {useState} from 'react'
import {UserProvider} from './UserContext';

// import {Fragment} from 'react';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router} from 'react-router-dom';
import {Routes, Route} from 'react-router-dom';
import AppNavbar from './components/AppNavbar'; 
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
import Courses from './pages/Courses';
import Home from './pages/Home'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import PageNotFound from './pages/PageNotFound'

import './App.css';

function App() {

  const [user,setUser] = useState ({
    email: localStorage.getItem('email')
  })

// Function to clear the localStorage for logout
  const unsetUser = () => {
    localStorage.clear()
  }


  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container>
          <Routes>
            <Route exact path="/" element={<Home/>}/>
            <Route exact path="/courses" element={<Courses/>}/>
            <Route exact path="/login" element={<Login/>}/>
            <Route exact path="/logout" element={<Logout/>}/>
            <Route exact path="/register" element={<Register/>}/>
            <Route exact path="*" element={<PageNotFound/>}/>
          </Routes>
        </Container>
      </Router>
    </UserProvider>
    
  );
}

export default App;
